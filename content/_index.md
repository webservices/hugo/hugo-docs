---
title: "HUGO for CERN documentation"
date: 2020-05-13T09:42:10+02:00
draft: false
---

This guide provides information about how to create a Markdown-based static site with [HUGO](https://gohugo.io/).