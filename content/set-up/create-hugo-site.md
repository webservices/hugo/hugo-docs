---
title: "Create a HUGO site from scratch"
date: 2020-05-13T09:42:10+02:00
draft: false
---

This post will explain users how to create a HUGO site and fill some content on it.

## Pre-requisites

First we need to download the client from [https://github.com/gohugoio/hugo/releases](https://github.com/gohugoio/hugo/releases).

We installed locally in our computer and check that works properly by running:

```bash
hugo version
# Hugo Static Site Generator vx.xx.x/extended windows/amd64 BuildDate: unknown
```

## Quickstart

In order to setup our HUGO site, we can choose for the following two options.

Generate the directory structure with the following command.

```bash
hugo new site mysite
```

This will create the corresponding directory structure, that will show like this.

```bash
.
├── archetypes
├── config.toml
├── content
├── data
├── layouts
├── static
└── theme
```

Or by importing the project [https://gitlab.cern.ch/webservices/hugo/hugo-scaffold-project](https://gitlab.cern.ch/webservices/hugo/hugo-scaffold-project), which is an scaffold project ready to be used.