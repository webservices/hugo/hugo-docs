---
title: "Install a Theme"
date: 2020-05-13T09:42:10+02:00
draft: false
---

Install a theme is quite easy. We just need to go to [https://themes.gohugo.io/](https://themes.gohugo.io/), select one that looks nice and follow the instructions the theme gives for us.

Mainly, the steps to install a `theme` are.

```bash
my-site$
.
├── archetypes
├── config.toml
├── content
├── data
├── layouts
├── static
└── theme

git init
git submodule add https://github.com/my/awesome-theme.git theme/awesome-theme
```

Then, we need to set under the `config.(yaml|toml)` the following:

```yaml
theme: awesome-theme
```