---
title: "Local development"
date: 2020-05-13T09:42:10+02:00
draft: true
---

To develop your site locally before moving to production, consider executing the following command.

```bash
# This command replicates exactly the same as in the HUGO infrastructure.
my-site$ hugo server --baseURL "http://localhost/" --bind=0.0.0.0 --buildDrafts --port=1313 --disableLiveReload=true --watch=false --appendPort=true --log

# Building sites …
#                    | EN
# -------------------+-----
#   Pages            | 12
#   Paginator pages  |  0
#   Non-page files   |  0
#   Static files     | 26
#   Processed images |  0
#   Aliases          |  0
#   Sitemaps         |  1
#   Cleaned          |  0

# Built in 404 ms
# Watching for changes in D:\my-site\{archetypes,content,data,layouts,static,themes}
# Watching for config changes in D:\my-site\config.toml
# Environment: "development"
# Serving pages from memory
# Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
# Web Server is available at http://localhost:1313/ (bind address 0.0.0.0)
# Press Ctrl+C to stop


# If we want to perform changes and reload them instantaneously, consider using the following
my-site$ hugo server --baseURL "http://localhost/" --bind=0.0.0.0 --buildDrafts --port=1313 --appendPort=true --log
```

Then just access to `http://localhost:1313` in your browser.